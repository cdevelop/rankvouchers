package com.cjoseph.rankvouchers.event;

import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.action.UserManager;
import com.cjoseph.rankvouchers.api.Voucher;
import com.cjoseph.rankvouchers.api.event.RVRedeemEvent;
import com.cjoseph.rankvouchers.util.ColorUtil;
import com.cjoseph.rankvouchers.util.WorldGuardHook;
import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;

public class VoucherInteractEvent implements Listener {
    private Core core;
    public VoucherInteractEvent(Core core){
        Bukkit.getPluginManager().registerEvents(this, core);
        this.core = core;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        Player player = e.getPlayer();
        if (!Bukkit.getVersion().contains("1.8")) {
            if (e.getHand() == EquipmentSlot.OFF_HAND) {
                return; // off hand packet, ignore.
            }
        }
        /**
         * Check Regions
         */
        Voucher voucher = Voucher.getByItem(player.getInventory().getItemInHand());
        if(voucher != null){
            if(!player.hasPermission("voucher.bypass")){
                UserManager userManager = core.getUser(player.getUniqueId());

                if(voucher.hasPermission() && !player.hasPermission("voucher.use." + voucher.getId().toLowerCase())){
                    e.setCancelled(true);
                    player.sendMessage(core.getVoucherNoPermission());
                    return;
                }

                if(!voucher.getValidWorlds().contains(player.getWorld().getName())){
                    e.setCancelled(true);
                    player.sendMessage(core.getWorldNoPermission());
                    return;
                }

                if(voucher.getLimit() > 0 && userManager.getConfig().getInt("voucher."+voucher.getId()) >= voucher.getLimit()){
                    e.setCancelled(true);
                    player.sendMessage(ColorUtil.translate(core.getConfig().getString("message.limit")
                    .replace("%type%", voucher.getId()).replace("%limit%", voucher.getLimit()+"")));
                    return;
                }

                if(core.getServer().getPluginManager().isPluginEnabled("WorldGuard")){
                    if(voucher.getValidRegions().size() > 0){
                        for (String r : voucher.getValidRegions()) {
                            if (!WorldGuardHook.checkIfPlayerInRegion(player, r)) {
                                player.sendMessage(ColorUtil.translate(core.getConfig().getString("message.no-permission.region")));
                                return;
                            }
                        }
                    }
                }

            }
            Bukkit.getPluginManager().callEvent(new RVRedeemEvent(player, voucher));
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onVoucherRedeem(RVRedeemEvent e){
        Player player = e.getPlayer();
        Voucher voucher = e.getVoucher();

        UserManager userManager = core.getUser(player.getUniqueId());
        int voucherCount = userManager.getConfig().getInt("voucher."+voucher.getId()) + 1;
        userManager.getConfig().set("voucher."+voucher.getId().toLowerCase(), voucherCount);
        userManager.save();

        ItemStack hand = player.getInventory().getItemInHand();
        if(hand.getAmount() == 1) player.setItemInHand(new ItemStack(Material.AIR));
        else hand.setAmount(hand.getAmount() - 1);

        voucher.getCumulativeRewards().forEach((key, reward) -> {
            if(reward.getRequirement() == voucherCount){
                core.getActionManager().runActions(reward.getRewards(), player);
            }
        });

        AtomicBoolean hasReward = new AtomicBoolean(false);
        while(!hasReward.get()){
            voucher.getRedeemRewards().forEach((key, reward) -> {
                ThreadLocalRandom random = ThreadLocalRandom.current();
                int rand = random.nextInt(100);

                if(!hasReward.get()){
                    if (rand <= reward.getChance()) {
                        core.getActionManager().runActions(reward.getRewards(), player);
                        hasReward.set(true);
                    }
                }
            });
        }
    }
}
