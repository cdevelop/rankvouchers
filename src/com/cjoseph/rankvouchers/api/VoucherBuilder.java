package com.cjoseph.rankvouchers.api;

import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.util.ColorUtil;
import com.cjoseph.rankvouchers.util.Glow;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class VoucherBuilder {
    private String name;
    private List<String> lore;
    private Material type;
    private List<String> enchants;
    private List<String> itemFlags;
    private int data;
    private boolean unbreakable;
    private boolean glow;
    private String id;

    private Core core;

    public VoucherBuilder(String id){
        this.name = id + " Voucher";
        this.lore = new ArrayList<>();
        this.enchants = new ArrayList<>();
        this.itemFlags = new ArrayList<>();
        this.id = id;
        this.core = JavaPlugin.getPlugin(Core.class);
        this.unbreakable = false;
        this.glow = glow;
    }

    public VoucherBuilder withType(String type){
        Material mat;
        try {
            mat = Material.valueOf(type);
        } catch (NullPointerException e){
            core.getLogger().warning("Material '" + type + "' for '" + id + "' voucher is invalid. Defaulting to Paper!");
            mat = Material.PAPER;
        }
        this.type = mat;
        return this;
    }

    public VoucherBuilder withName(String name){
        if(name != null)
            this.name = ColorUtil.translate(name);
        return this;
    }

    public VoucherBuilder withData(int data){
        this.data = data;
        return this;
    }

    public VoucherBuilder withLore(List<String> lore){
        if(lore != null)
            this.lore = ColorUtil.translate(lore);
        return this;
    }

    public VoucherBuilder withEnchants(List<String> enchants){
        if(enchants != null)
            this.enchants = enchants;
        return this;
    }

    public VoucherBuilder withItemFlags(List<String> itemFlags){
        if(itemFlags != null)
            this.itemFlags = itemFlags;
        return this;
    }

    public VoucherBuilder withUnbreakable(boolean unbreakable){
        this.unbreakable = unbreakable;
        return this;
    }
    public VoucherBuilder withGlow(boolean glow){
        this.glow = glow;
        return this;
    }


    public ItemStack getItemStack(){
        ItemStack item = new ItemStack(type);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(name);
        meta.setLore(lore);

        itemFlags.forEach(flag -> {
            try {
                meta.addItemFlags(ItemFlag.valueOf(flag.toUpperCase()));
            } catch (IllegalArgumentException e){
                core.getLogger().warning("Item Flag '" + flag + "' is invalid. Skipping!");
            }
        });


        if(unbreakable)
            meta.spigot().setUnbreakable(true);

        item.setDurability((short) data);
        item.setItemMeta(meta);

        enchants.forEach(enchant -> {
            String[] str = enchant.toUpperCase().split(";");
            Enchantment enchantment = Enchantment.getByName(str[0]);

            if(enchantment == null){
                core.getLogger().warning("Enchantment '" + str[0] + "' for '" + id + "' voucher is invalid. Skipping!");
            } else {
                item.addUnsafeEnchantment(enchantment, Integer.valueOf(str[1]));
            }
        });

        if(glow){
            item.addUnsafeEnchantment(core.registerGlow(), 1);
        }

        return item;
    }
}
