package com.cjoseph.rankvouchers.api;

import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.util.ColorUtil;
import com.google.common.collect.Maps;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public class Voucher {
    private String id;
    private ItemStack item;
    private Map<String, RedeemReward> redeemRewards;
    private Map<String, CumulativeReward> cumulativeRewards;
    private boolean hasPermission;
    private List<String> validWorlds;
    private List<String> validRegions;
    private int limit;
    private Core core;

    public Voucher(String id){
        redeemRewards = new HashMap<>();
        this.id = id;
        this.core = JavaPlugin.getPlugin(Core.class);
        this.hasPermission = false;
        this.validWorlds = new ArrayList<>();
        this.validRegions = new ArrayList<>();
        this.redeemRewards = Maps.newHashMap();
        this.cumulativeRewards = Maps.newHashMap();
        this.limit = 0;
    }

    public Voucher withItem(ItemStack item) {
        this.item = item;
        return this;
    }

    public Voucher withPermission(boolean hasPermission){
        this.hasPermission = hasPermission;
        return this;
    }

    public Voucher withWorlds(List<String> validWorlds){
        if(validWorlds != null && validWorlds.size() > 0)
            this.validWorlds = validWorlds;
        return this;
    }
    public Voucher withRegions(List<String> validRegions){
        if(validRegions != null && validRegions.size() > 0)
            this.validRegions = validRegions;
        return this;
    }

    public Voucher withLimit(int limit){
        this.limit = limit;
        return this;
    }

    public Voucher withRedeemRewards(ConfigurationSection rewards, YamlConfiguration config){
        if(rewards == null){
            core.getLogger().warning("Redeem Rewards for '" + id + "' voucher is invalid. Skipping!");
            redeemRewards.put("not-exist", new RedeemReward(Collections.singletonList("[Title] &c&lError;&7Rewards not found"), 100));
            return this;
        }
        rewards.getKeys(false).forEach(reward -> redeemRewards.put(reward, new RedeemReward(config.getStringList("redeem-rewards."+reward+".reward"),
                config.getInt("redeem-rewards."+reward+".chance"))));
        return this;
    }

    public Voucher withRedeemRewards(Map<String,RedeemReward> rewards){
        if(rewards == null){
            core.getLogger().warning("Redeem Rewards for '" + id + "' voucher is invalid. Skipping!");
            redeemRewards.put("not-exist", new RedeemReward(Collections.singletonList("[Title] &c&lError;&7Rewards not found"), 100));
            return this;
        }
        rewards.forEach((key, value) -> redeemRewards.put(key, value));
        return this;
    }


    public Voucher withCumulativeRewards(ConfigurationSection rewards, YamlConfiguration config){
        if(rewards == null){
            return this;
        }
        rewards.getKeys(false).forEach(reward -> cumulativeRewards.put(reward, new CumulativeReward(config.getStringList("cumulative-rewards."+reward+".reward"),
                config.getInt("cumulative-rewards."+reward+".requirement"))));
        return this;
    }

    public Voucher withCumulativeRewards(Map<String,CumulativeReward> rewards){
        if(rewards == null){
            return this;
        }
        rewards.forEach((key, value) -> cumulativeRewards.put(key, value));
        return this;
    }

    public void save(){
        core.getManager().addVoucher(this);
    }

    public String getId(){
        return id;
    }

    public ItemStack getItem() {
        return item;
    }

    public Map<String, RedeemReward> getRedeemRewards() {
        return redeemRewards;
    }

    public Map<String, CumulativeReward> getCumulativeRewards() {
        return cumulativeRewards;
    }

    public boolean hasPermission() {
        return hasPermission;
    }

    public List<String> getValidWorlds() {
        return validWorlds;
    }

    public List<String> getValidRegions() {
        return validRegions;
    }

    public int getLimit() {
        return limit;
    }

    public static void giveVoucher(Voucher voucher, Player target, Integer amount){
        Core core = JavaPlugin.getPlugin(Core.class);
        ItemStack item = voucher.getItem();
        item.setAmount(amount);
        ItemMeta meta = item.getItemMeta();
        meta.setLore(core.addPlaceholders(meta.getLore(), target.getPlayer()));
        item.setItemMeta(meta);

        target.getPlayer().getInventory().addItem(item);
        target.getPlayer().sendMessage(ColorUtil.translate(core.getConfig().getString("message.received")
                .replace("%type%", voucher.getId())
                .replace("%amount%", amount+"")
        ));
    }

    public static Voucher getByItem(ItemStack is){
        Optional<Map.Entry<String, Voucher>> voucher = VoucherManager.getVouchers().entrySet().stream().filter(v -> v.getValue().getItem().isSimilar(is))
                .findFirst();

        if(voucher.isPresent()){
            return voucher.get().getValue();
        } else {
            return null;
        }

    }
}
