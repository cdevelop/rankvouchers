package com.cjoseph.rankvouchers.api.event;

import com.cjoseph.rankvouchers.api.Voucher;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RVRedeemEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private Voucher voucher;
    private boolean cancelled;

    public RVRedeemEvent(Player player, Voucher voucher){
        this.player = player;
        this.voucher = voucher;
    }

    public Player getPlayer(){
        return player;
    }

    public Voucher getVoucher(){
        return voucher;
    }

    public HandlerList getHandlers(){
        return handlers;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }
    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
