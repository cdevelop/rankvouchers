package com.cjoseph.rankvouchers.api;

import java.util.List;

public class CumulativeReward {
    private List<String> rewards;
    private int requirement;

    public CumulativeReward(List<String> rewards, int requirement){
        this.rewards = rewards;
        this.requirement = requirement;
    }

    public List<String> getRewards() {
        return rewards;
    }

    public int getRequirement() {
        return requirement;
    }
}
