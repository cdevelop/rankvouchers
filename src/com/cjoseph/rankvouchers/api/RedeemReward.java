package com.cjoseph.rankvouchers.api;

import java.util.List;

public class RedeemReward {
    private List<String> rewards;
    private int chance;

    public RedeemReward(List<String> rewards, int chance){
        this.rewards = rewards;
        this.chance = chance;
    }

    public List<String> getRewards() {
        return rewards;
    }

    public int getChance() {
        return chance;
    }
}
