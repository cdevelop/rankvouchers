package com.cjoseph.rankvouchers.api;

import com.cjoseph.rankvouchers.Core;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class VoucherManager {

    private static Map<String, Voucher> voucherMap;
    private Core core;
    public VoucherManager(Core core){
        voucherMap = new HashMap<>();
        this.core = core;
    }

    public static Map<String, Voucher> getVouchers(){
        return voucherMap;
    }

    public static Voucher getVoucher(String id){
        for(Map.Entry<String, Voucher> voucherMap : getVouchers().entrySet()){
            if(voucherMap.getKey().equalsIgnoreCase(id)){
                return voucherMap.getValue();
            }
        }
        return null;
    }

    public static ConfigurationSection createConfigSection(String id){
        return JavaPlugin.getPlugin(Core.class).getConfig().createSection(id);
    }

    public void addVoucher(Voucher voucher){
        voucherMap.put(voucher.getId(), voucher);
    }
    public void removeVoucher(Voucher voucher){
        voucherMap.put(voucher.getId(), voucher);
    }
}
