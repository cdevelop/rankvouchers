package com.cjoseph.rankvouchers;

import com.cjoseph.rankvouchers.action.UserManager;
import com.cjoseph.rankvouchers.api.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Example {

    public void onCreate(){
        Map<String, Voucher> voucherList = VoucherManager.getVouchers();
        Voucher voucherById = VoucherManager.getVoucher("<identifier>");

        Core voucherCore = (Core) JavaPlugin.getProvidingPlugin(Core.class);
       // UserManager userByUuid = voucherCore.getUser(UUID);
        List<UserManager> userList = voucherCore.getUserManager();
        VoucherManager voucherManager = voucherCore.getManager();
    }

    public void defineVoucher(){
        // Create Voucher with Identifier 'example'
        Voucher voucher = new Voucher("example");

        // Create Voucher ItemStack - Using our Builder
        VoucherBuilder voucherBuilder = new VoucherBuilder("example")
                .withName("&bExample Voucher")
                .withLore(Arrays.asList("&7RV Example Voucher", "", "&eCool, Right?"))
                .withType("PAPER")
                .withData(0)
                .withGlow(true)
                .withEnchants(Arrays.asList("DURABILITY;1", "DIG_SPEED;2"))
                .withItemFlags(Arrays.asList("HIDE_ENCHANTS", "HIDE_ATTRIBUTES"))
                .withUnbreakable(true);

        // Define Our Voucher Settings + Add ItemStack
        voucher.withItem(voucherBuilder.getItemStack())
                .withLimit(3)
                .withRegions(Arrays.asList("region_one", "region_two"))
                .withWorlds(Arrays.asList("first_world", "second_world"))
                .withPermission(true);

        // Define Our Redeem Rewards (On Redeem)
        Map<String, RedeemReward> rewardMap = new HashMap<>();
        rewardMap.put("tier1", new RedeemReward(Arrays.asList("[Title] &aSimple Example;&7You can add more!", "[Message] Woah!"), 80));
        rewardMap.put("tier2", new RedeemReward(Arrays.asList("[Title] &aAnother Example;&7You can add many more!", "[Message] Woah another!"), 20));

        // Define Our Cumulative Rewards (When x Voucher Redeemed)
        Map<String, CumulativeReward> cumulativeMap = new HashMap<>();
        cumulativeMap.put("premium", new CumulativeReward(Arrays.asList("[Title] &a5 Redeemed!", "[Message] You've redeemed 5 vouchers of this type. Congrats!"), 5));
        cumulativeMap.put("advanced", new CumulativeReward(Arrays.asList("[Title] &a10 Redeemed!", "[Message] You've redeemed 10 vouchers of this type. Dang, Congrats!"), 10));

        // Set Our Voucher Settings
        voucher.withRedeemRewards(rewardMap).withCumulativeRewards(cumulativeMap);

        // Build Our Voucher
        voucher.save();
    }
}
