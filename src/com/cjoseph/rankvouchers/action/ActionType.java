package com.cjoseph.rankvouchers.action;

/**
 * Created by Chazmondo
 */
public enum ActionType {
    PLAYER_COMMAND,
    CONSOLE_COMMAND,
    MESSAGE,
    BROADCAST,
    TITLE,
    SUBTITLE,
    SOUND,
    ACTION_BAR
}
