package com.cjoseph.rankvouchers.action;

import com.cjoseph.rankvouchers.Core;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class UserManager {
    private UUID uuid;
    private File file;
    private YamlConfiguration config;
    private Core core;

    public UserManager(UUID userId){
        this.core = JavaPlugin.getPlugin(Core.class);
        this.file = new File(core.getDataFolder() + File.separator + "users", userId.toString() + ".yml");
        this.uuid = userId;

        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.config = YamlConfiguration.loadConfiguration(file);
        core.getUserManager().add(this);
    }

    public Core getCore() {
        return core;
    }

    public File getFile() {
        return file;
    }

    public UUID getUuid() {
        return uuid;
    }

    public YamlConfiguration getConfig() {
        return config;
    }

    public void save(){
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        config = YamlConfiguration.loadConfiguration(file);
    }
}
