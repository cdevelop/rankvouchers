package com.cjoseph.rankvouchers;

import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.BukkitMessageFormatter;
import co.aikar.commands.MessageType;
import com.cjoseph.rankvouchers.action.Action;
import com.cjoseph.rankvouchers.action.ActionManager;
import com.cjoseph.rankvouchers.action.ActionType;
import com.cjoseph.rankvouchers.action.UserManager;
import com.cjoseph.rankvouchers.api.Voucher;
import com.cjoseph.rankvouchers.api.VoucherBuilder;
import com.cjoseph.rankvouchers.api.VoucherManager;
import com.cjoseph.rankvouchers.command.*;
import com.cjoseph.rankvouchers.event.VoucherInteractEvent;
import com.cjoseph.rankvouchers.util.ColorUtil;
import com.cjoseph.rankvouchers.util.Glow;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.*;

public class Core extends JavaPlugin {
    private VoucherManager voucherManager;
    public VoucherManager getManager() {
        return voucherManager;
    }

    private List<UserManager> userManager;
    public List<UserManager> getUserManager() {
        return userManager;
    }

    public UserManager getUser(UUID user){
        Optional<UserManager> userConfig = userManager.stream().filter(uuid -> uuid.getUuid() == user).findFirst();
        return userConfig.orElseGet(() -> new UserManager(user));
    }

    private ActionManager actionManager;
    public ActionManager getActionManager() {
        return actionManager;
    }

    private void checkInstalled(String plugin){
        if(getServer().getPluginManager().isPluginEnabled(plugin))
            log("Hooked into " + plugin + "!");
    }

    @Override
    public void onEnable(){
        voucherManager = new VoucherManager(this);
        log("---------------------------");
        setupConfig();
        setupVouchers();
        log("---------------------------");


        checkInstalled("PlaceholderAPI");
        checkInstalled("WorldGuard");

        log(" ");
        log("Developed by Setups.io.");

        int count = VoucherManager.getVouchers().size();
        String voucherCount = count > 1 ? " Vouchers." : "Voucher.";

        if(count > 0) log("Ready! " + (count + voucherCount));
        else log("Ready!");
        log(" ");

        BukkitCommandManager bcm = new BukkitCommandManager(this);
        bcm.setFormat(MessageType.SYNTAX, new BukkitMessageFormatter(ChatColor.GRAY, ChatColor.WHITE, ChatColor.AQUA));
        bcm.getCommandReplacements().addReplacement("rootCommand", "rankvouchers|rvouchers|rv|voucher|vouchers");
        bcm.getCommandCompletions().registerCompletion("vouchers", c -> {
            Collection<String> vouchers = new ArrayList<>();
            VoucherManager.getVouchers().forEach((k, v) -> vouchers.add(v.getId()));
            return vouchers;
        });

        new VoucherInteractEvent(this);

        new MainCommand(bcm, this);
        new HelpCommand(bcm, this);
        new ReloadCommand(bcm, this);
        new GiveCommand(bcm, this);
        new GiveAllCommand(bcm, this);
        new ListCommand(bcm, this);
        new CreateCommand(bcm, this);
        new RemoveCommand(bcm, this);

        userManager = new ArrayList<>();
        actionManager = new ActionManager(this);
        actionManager.addAction(new Action("[CONSOLE]").withType(ActionType.CONSOLE_COMMAND).compile());
        actionManager.addAction(new Action("[PLAYER]").withType(ActionType.PLAYER_COMMAND).compile());
        actionManager.addAction(new Action("[MESSAGE]").withType(ActionType.MESSAGE).compile());
        actionManager.addAction(new Action("[BROADCAST]").withType(ActionType.BROADCAST).compile());
        actionManager.addAction(new Action("[ACTIONBAR]").withType(ActionType.ACTION_BAR).compile());
        actionManager.addAction(new Action("[TITLE]").withType(ActionType.TITLE).compile());
        actionManager.addAction(new Action("[SUBTITLE]").withType(ActionType.SUBTITLE).compile());
        actionManager.addAction(new Action("[SOUND]").withType(ActionType.SOUND).compile());
    }

    public String addPlaceholders(String str, Player p){
        if(getServer().getPluginManager().isPluginEnabled("PlaceholderAPI")) str = PlaceholderAPI.setPlaceholders(p, str);
        str = str.replace("%player%", p.getName())
                .replace("%version%", getDescription().getVersion());
        return ColorUtil.translate(str);
    }

    public List<String> addPlaceholders(List<String> str, Player p){
        List<String> replacedList = new ArrayList<>();
        str.forEach(msg -> replacedList.add(addPlaceholders(msg, p)));
        return replacedList;
    }

    private void log(String message){
        getLogger().info(message);
    }

    public void handleReload(){
        reloadConfig();
        setupConfig();
        setupVouchers();
        userManager = new ArrayList<>();
    }

    public String getCmdNoPermission(){
        return ColorUtil.translate(getConfig().getString("message.no-permission.command"));
    }

    public String getVoucherNoPermission(){
        return ColorUtil.translate(getConfig().getString("message.no-permission.voucher"));
    }

    public String getWorldNoPermission(){
        return ColorUtil.translate(getConfig().getString("message.no-permission.world"));
    }

    private void setupConfig(){
        FileConfiguration c = this.getConfig();
        c.options().header("-----[ Rank Vouchers ]----- #\r\n---------[ v"
                + getDescription().getVersion() + " ]--------- #");
        c.addDefault("message.no-permission.command", "&bRankVouchers: &7You do not have permission to do that.");
        c.addDefault("message.no-permission.voucher", "&bRankVouchers: &7You require &cvoucher.use.%type% &7to use that voucher.");
        c.addDefault("message.no-permission.world", "&bRankVouchers: &7You cannot use that voucher in this world.");
        c.addDefault("message.no-permission.region", "&bRankVouchers: &7You cannot use that voucher in this region.");
        c.addDefault("message.invalid", "&bRankVouchers: &7The voucher specified does not exist.");
        c.addDefault("message.received", "&bRankVouchers: &7You've received &f%amount%x &3%type% &7Voucher&7.");
        c.addDefault("message.limit", "&bRankVouchers: &7Limit of &c%limit% &7reached for &b%type% &7voucher.");
        c.addDefault("message.command.list", "&bRankVouchers: &7Listing &f%amount% &7Vouchers:");
        c.addDefault("message.command.main", Arrays.asList("&bRankVouchers: &7Running &fv%version% &7by &cChazmondo.", "&bRankVouchers: &7Use &f/voucher help &7for commands."));
        c.addDefault("message.command.help", Arrays.asList("", "&bRankVouchers: &7Listing Commands:", "",
                " &8# &f/voucher help", " &8# &f/voucher give <player> <voucher> [amount]", " &8# &f/voucher giveall <voucher> [amount]",
                " &8# &f/voucher list", ""));
        c.addDefault("message.command.reload", "&bRankVouchers: &7Successfully reloaded, &f%amount% &7vouchers registered.");
        c.addDefault("message.command.given", "&bRankVouchers: &c%player% &7has been given &f%amount%x &3%type% &7Voucher&7.");
        c.addDefault("message.command.given-all", "&bRankVouchers: &c%online% Online Players &7have been given &f%amount%x &3%type% &7Voucher&7.");
        c.addDefault("message.command.created", "&bRankVouchers: &7Created &b%type% &7voucher.");
        c.addDefault("message.command.removed", "&bRankVouchers: &7Removed &b%type% &7voucher.");
        c.addDefault("format.list", " &8- &f%type%");

        c.options().copyDefaults(true);
        saveConfig();

        File userData = new File(getDataFolder() + File.separator + "users");
        userData.mkdirs();
    }

    public Enchantment registerGlow() {
        Field f;
        int id = 420;
        Enchantment glow = Enchantment.getById(id);
        if (glow != null) {
            if (glow.getName().equals("RV Glow")) {
                return glow;
            }
        }
        Boolean forced = false;
        if (!Enchantment.isAcceptingRegistrations()) {
            try {
                f = Enchantment.class.getDeclaredField("acceptingNew");
                f.setAccessible(true);
                f.set(null, true);
                forced = true;
            }
            catch (Exception ignored) {}
        }
        try {
            glow = new Glow(100);
            Enchantment.registerEnchantment(glow);
        }
        catch (Exception ignored) {}
        if (forced) {
            try {
                f = Enchantment.class.getDeclaredField("acceptingNew");
                f.set(null, false);
                f.setAccessible(false);
            }
            catch (Exception ignored) {}
        }
        return glow;
    }

    public void setupVouchers(){
        File vouchers = new File(getDataFolder() + File.separator + "vouchers");

        if(vouchers.isDirectory() && vouchers.exists()){
            YamlConfiguration config = new YamlConfiguration();
            try {
                Files.newDirectoryStream(vouchers.toPath(),
                        path -> path.toString().endsWith(".yml"))
                        .forEach(f -> {
                            String identifier = f.getFileName().toString().replace(".yml", "");
                            try {
                                config.load(f.toFile());
                                log("Configuring: " + identifier);

                                VoucherBuilder builder = new VoucherBuilder(identifier)
                                        .withType(config.getString("item.type"))
                                        .withName(config.getString("meta.name"))
                                        .withLore(config.getStringList("meta.lore"))
                                        .withData(config.getInt("item.data"))
                                        .withItemFlags(config.getStringList("meta.flags"))
                                        .withEnchants(config.getStringList("meta.enchants"))
                                        .withUnbreakable(config.getBoolean("meta.unbreakable"))
                                        .withGlow(config.getBoolean("meta.glow"));

                                Voucher voucher = new Voucher(identifier)
                                        .withItem(builder.getItemStack())
                                        .withPermission(config.getBoolean("setting.permission"))
                                        .withWorlds(config.getStringList("setting.world"))
                                        .withRegions(config.getStringList("setting.region"))
                                        .withLimit(config.getInt("setting.limit"))
                                        .withRedeemRewards(config.getConfigurationSection("redeem-rewards"), config)
                                        .withCumulativeRewards(config.getConfigurationSection("cumulative-rewards"), config);

                                voucher.save();

                            } catch (IOException | InvalidConfigurationException e) {
                                log("Error - Please submit the below to Chazmondo on Spigot.");
                                e.printStackTrace();
                            }
                        });
            } catch (IOException e) {
                log("Error - Please submit the below to Chazmondo on Spigot.");
                e.printStackTrace();
            }
        } else {
            vouchers.mkdirs();
            log("Creating directory at " + vouchers.getPath() + ".");
            setupVouchers();
        }
    }
}
