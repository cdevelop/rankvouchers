package com.cjoseph.rankvouchers.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.annotation.Values;
import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.api.VoucherManager;
import com.cjoseph.rankvouchers.util.ColorUtil;
import org.apache.commons.lang.WordUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

@CommandAlias("%rootCommand")
public class RemoveCommand extends BaseCommand {
    private Core core;
    public RemoveCommand(BukkitCommandManager bcm, Core core){
        bcm.registerCommand(this, true);
        this.core = core;
    }

    @Subcommand("remove")
    @CommandCompletion("@vouchers")
    public void onCommand(CommandSender cs, @Values("@vouchers") String id){
        if(cs.hasPermission("voucher.cmd.remove")){
            if (VoucherManager.getVouchers().entrySet().stream().noneMatch(voucherEntry -> voucherEntry.getKey().equalsIgnoreCase(id))) {
                cs.sendMessage(ColorUtil.translate(core.getConfig().getString("message.invalid")));
                return;
            }
            File file = new File(core.getDataFolder() + File.separator + "vouchers",id + ".yml");
            file.delete();
            cs.sendMessage(ColorUtil.translate(core.getConfig().getString("message.command.removed").replace("%type%", id)));
            core.setupVouchers();
        } else cs.sendMessage(core.getCmdNoPermission());
    }
}
