package com.cjoseph.rankvouchers.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.annotation.*;
import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.api.VoucherManager;
import com.cjoseph.rankvouchers.api.Voucher;
import com.cjoseph.rankvouchers.util.ColorUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

@CommandAlias("%rootCommand")
public class GiveAllCommand extends BaseCommand {
    private Core core;
    public GiveAllCommand(BukkitCommandManager bcm, Core core){
        bcm.registerCommand(this, true);
        this.core = core;
    }

    @Subcommand("giveall")
    @CommandCompletion("@vouchers")
    public void onCommand(CommandSender cs, @Values("@vouchers") String id, @Optional @Default("1") Integer amount){
        if(cs.hasPermission("voucher.cmd.giveall")){
            Voucher voucher = VoucherManager.getVoucher(id);
            if(voucher != null){
                Bukkit.getOnlinePlayers().forEach(p -> Voucher.giveVoucher(voucher, p.getPlayer(), amount));
                cs.sendMessage(ColorUtil.translate(core.getConfig().getString("message.command.given-all")
                        .replace("%online%", Bukkit.getOnlinePlayers().size()+"")
                        .replace("%type%", voucher.getId())
                        .replace("%amount%", amount+"")
                ));
            } else cs.sendMessage(ColorUtil.translate(core.getConfig().getString("message.invalid")));
        } else cs.sendMessage(core.getCmdNoPermission());
    }
}
