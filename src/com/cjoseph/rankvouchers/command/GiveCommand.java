package com.cjoseph.rankvouchers.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.annotation.*;
import co.aikar.commands.contexts.OnlinePlayer;
import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.api.Voucher;
import com.cjoseph.rankvouchers.api.VoucherManager;
import com.cjoseph.rankvouchers.util.ColorUtil;
import org.bukkit.command.CommandSender;

@CommandAlias("%rootCommand")
public class GiveCommand extends BaseCommand {
    private Core core;
    public GiveCommand(BukkitCommandManager bcm, Core core){
        bcm.registerCommand(this, true);
        this.core = core;
    }

    @Subcommand("give")
    @CommandCompletion("@players @vouchers")
    public void onCommand(CommandSender cs, OnlinePlayer target, @Values("@vouchers") String id, @Optional @Default("1") Integer amount){
        if(cs.hasPermission("voucher.cmd.give")){
            Voucher voucher = VoucherManager.getVoucher(id);
            if(voucher != null){
                Voucher.giveVoucher(voucher, target.getPlayer(), amount);
                if(cs != target.getPlayer())
                    cs.sendMessage(ColorUtil.translate(core.getConfig().getString("message.command.given")
                            .replace("%player%", target.getPlayer().getName())
                            .replace("%type%", voucher.getId())
                            .replace("%amount%", amount+"")
                    ));

            } else {
                cs.sendMessage(ColorUtil.translate(core.getConfig().getString("message.invalid")));
            }
        } else cs.sendMessage(core.getCmdNoPermission());
    }
}
