package com.cjoseph.rankvouchers.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.annotation.CommandAlias;
import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.util.ColorUtil;
import org.bukkit.command.CommandSender;

public class MainCommand extends BaseCommand {
    private Core core;
    public MainCommand(BukkitCommandManager bcm, Core core){
        bcm.registerCommand(this, true);
        this.core = core;
    }

    @CommandAlias("%rootCommand")
    public void onCommand(CommandSender cs){
        core.getConfig().getStringList("message.command.main")
                .forEach(msg -> cs.sendMessage(ColorUtil.translate(msg.replace("%version%", core.getDescription().getVersion()))));
        //core.addPlaceholders(core.getConfig().getStringList("message.command.main"), p).forEach(p::sendMessage);
    }
}
