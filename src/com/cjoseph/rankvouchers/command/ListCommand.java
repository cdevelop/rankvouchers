package com.cjoseph.rankvouchers.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.annotation.*;
import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.api.VoucherManager;
import com.cjoseph.rankvouchers.util.ColorUtil;
import org.bukkit.command.CommandSender;

@CommandAlias("%rootCommand")
public class ListCommand extends BaseCommand {
    private Core core;
    public ListCommand(BukkitCommandManager bcm, Core core){
        bcm.registerCommand(this, true);
        this.core = core;
    }

    @Subcommand("list")
    @CommandCompletion("list")
    public void onCommand(CommandSender cs){
        if(cs.hasPermission("voucher.cmd.list")){
            cs.sendMessage(ColorUtil.translate(core.getConfig().getString("message.command.list").replace("%amount%",
                    VoucherManager.getVouchers().size()+"")));
            VoucherManager.getVouchers().forEach((k, v) -> cs.sendMessage(ColorUtil.translate(core.getConfig().getString("format.list"))
                    .replace("%type%", v.getId())));
        } else cs.sendMessage(core.getCmdNoPermission());
    }
}
