package com.cjoseph.rankvouchers.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.annotation.*;
import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.api.Voucher;
import com.cjoseph.rankvouchers.api.VoucherManager;
import com.cjoseph.rankvouchers.util.ColorUtil;
import org.apache.commons.lang.WordUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

@CommandAlias("%rootCommand")
public class CreateCommand extends BaseCommand {
    private Core core;
    public CreateCommand(BukkitCommandManager bcm, Core core){
        bcm.registerCommand(this, true);
        this.core = core;
    }

    @Subcommand("create")
    @CommandCompletion("create")
    public void onCommand(CommandSender cs, String id){
        if(cs.hasPermission("voucher.cmd.create")){
            if (VoucherManager.getVouchers().entrySet().stream().anyMatch(voucherEntry -> voucherEntry.getKey().equalsIgnoreCase(id))) {
                cs.sendMessage(ColorUtil.translate(core.getConfig().getString("message.invalid")));
                return;
            }

            File file = new File(core.getDataFolder() + File.separator + "vouchers",id + ".yml");
            try {
                file.createNewFile();
            } catch (IOException e) {
                core.getLogger().warning("Error: " + e.getLocalizedMessage() + ".");
                e.printStackTrace();
            }
            FileConfiguration c = YamlConfiguration.loadConfiguration(file);

            c.options().header("---------[ ID: "
                    + id + " ]--------- #"
            + "\r\nDocumentation: https://pluginwiki.com/rankvouchers");
            c.addDefault("item.type", "PAPER");
            c.addDefault("item.data", 0);

            c.addDefault("meta.name", "&b&l" + WordUtils.capitalizeFully(id) + " &fVoucher");
            c.addDefault("meta.lore", Arrays.asList("&7Default Item.", "", "&a&nRight Click it"));
            c.addDefault("meta.enchants", Collections.singletonList("DURABILITY;1"));
            c.addDefault("meta.flags", Arrays.asList("HIDE_ATTRIBUTES", "HIDE_UNBREAKABLE"));
            c.addDefault("meta.glow", false);
            c.addDefault("meta.unbreakable", true);

            c.addDefault("redeem-rewards.average.reward",
                    Arrays.asList("[Broadcast] &a%player% &7used a &b" + id + " &7voucher.",
                            "[ActionBar] &7Woah, &a%player%&7!",
                            "[Subtitle] &eVoucher - " + id,
                            "[Message] &7Actions also support PlaceholderAPI! (You've got &a%player_exp_to_level% XP Levels&7)",
                            "[Message] &bRankVouchers: &7Go configure it!"));
            c.addDefault("redeem-rewards.average.chance", 70);

            c.addDefault("redeem-rewards.rare.reward",
                    Arrays.asList("[Broadcast] &a%player% &7used a &b" + id + " &7voucher.",
                            "[ActionBar] &7Woah, &a%player%&7!",
                            "[Subtitle] &e&k:::&r &6Rare RedeemReward &e&k:::&r"));
            c.addDefault("redeem-rewards.rare.chance", 30);

            c.addDefault("cumulative-rewards.tier1.reward",
                    Arrays.asList("[Broadcast] &a%player% &7has redeemed a &b" + id + " &7voucher &f5 times&7!",
                            "[Title] &aAchievement!;&fRedeemed more than 5 times!" + id,
                            "[Message] &7This is a Cumulative RedeemReward. Go configure it!"));
            c.addDefault("cumulative-rewards.tier1.requirement", 5);


            c.addDefault("setting.permission", false);
            c.addDefault("setting.limit", 0);
            c.addDefault("setting.region", Arrays.asList("worldguard_region", "another_good_region"));
            c.addDefault("setting.world", Arrays.asList("good_world", "another_good_world"));

            c.options().copyDefaults(true);
            try {
                c.save(file);
            } catch (IOException e) {
                core.getLogger().warning("Error: " + e.getLocalizedMessage() + ".");
                e.printStackTrace();
            }
            cs.sendMessage(ColorUtil.translate(core.getConfig().getString("message.command.created").replace("%type%", id)));
            core.setupVouchers();

        } else cs.sendMessage(core.getCmdNoPermission());
    }
}
