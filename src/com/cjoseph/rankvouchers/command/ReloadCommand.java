package com.cjoseph.rankvouchers.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Subcommand;
import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.api.VoucherManager;
import com.cjoseph.rankvouchers.util.ColorUtil;
import org.bukkit.command.CommandSender;

@CommandAlias("%rootCommand")
public class ReloadCommand extends BaseCommand {
    private Core core;
    public ReloadCommand(BukkitCommandManager bcm, Core core){
        bcm.registerCommand(this, true);
        this.core = core;
    }

    @Subcommand("reload")
    @CommandCompletion("reload")
    public void onCommand(CommandSender cs){
        if(cs.hasPermission("voucher.cmd.reload")){
            core.handleReload();
            cs.sendMessage(ColorUtil.translate(core.getConfig().getString("message.command.reload")
            .replace("%amount%", VoucherManager.getVouchers().size() + "")));
        } else cs.sendMessage(core.getCmdNoPermission());
    }
}
