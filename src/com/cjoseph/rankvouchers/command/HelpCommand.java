package com.cjoseph.rankvouchers.command;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Subcommand;
import com.cjoseph.rankvouchers.Core;
import com.cjoseph.rankvouchers.util.ColorUtil;
import org.bukkit.command.CommandSender;

@CommandAlias("%rootCommand")
public class HelpCommand extends BaseCommand {
    private Core core;
    public HelpCommand(BukkitCommandManager bcm, Core core){
        bcm.registerCommand(this, true);
        this.core = core;
    }

    @Subcommand("help")
    @CommandCompletion("help")
    public void onCommand(CommandSender cs){
        if(cs.hasPermission("voucher.cmd.help")){
            core.getConfig().getStringList("message.command.help")
                    .forEach(msg -> cs.sendMessage(ColorUtil.translate(msg)));
        } else cs.sendMessage(core.getCmdNoPermission());
    }
}
